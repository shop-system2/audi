package main

import (
	"context"
	"fmt"
	"net"
	"net/http"
	"os"
	"os/signal"
	"time"

	"audi/genproto/proto"
	"audi/internal/api"

	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

func main() {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()
	lisner, err := net.Listen("tcp", ":9090")
	if err != nil {
		panic(err)
	}

	go func() {
		grpcServer := grpc.NewServer()
		proto.RegisterAPIServer(grpcServer, api.NewApi())

		err = grpcServer.Serve(lisner)
		fmt.Println("grpcServer.Serve, err", err)
		if err != nil {
			panic(err)
		}
	}()

	mux := runtime.NewServeMux()
	opts := []grpc.DialOption{grpc.WithTransportCredentials(insecure.NewCredentials())}
	fmt.Println("start server grpc port: 9090")
	err = proto.RegisterAPIHandlerFromEndpoint(ctx, mux, ":9090", opts)
	if err != nil {
		panic(err)
	}
	httpMux := http.NewServeMux()
	httpMux.Handle("/", mux)

	ad := &http.Server{
		Addr:    ":8081",
		Handler: httpMux,
	}
	// Start HTTP server (and proxy calls to gRPC server endpoint)
	fmt.Println("start server http port: 8081")
	if err := ad.ListenAndServe(); err != nil {
		panic(err)
	}
	fmt.Println("*** start server ***")

	signals := make(chan os.Signal, 1)
	shutdown := make(chan bool, 1)
	signal.Notify(signals, os.Interrupt)
	go func() {
		<-signals
		_, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()
		shutdown <- true
	}()
	fmt.Println("*** shutdown ***")
	time.Sleep(3 * time.Second)
	<-shutdown

}
