folder: ## create folder in project
	@mkdir -p cmd
	@mkdir -p cmd/worker
	@mkdir -p cmd/server
	@mkdir -p config
	@mkdir -p db
	@mkdir -p docs
	@mkdir -p genproto
	@mkdir -p internal
	@mkdir -p internal/adapter
	@mkdir -p internal/facade
	@mkdir -p internal/api
	@mkdir -p internal/common
	@mkdir -p internal/dto
	@mkdir -p internal/helper
	@mkdir -p internal/registry
	@mkdir -p internal/repository
	@mkdir -p internal/usecase
	@mkdir -p internal/utils
	@mkdir -p thirdparty
	@mkdir -p proto
	@mkdir -p sql

gen:
	echo "gen protobuf"
	protoc --go_out=./genproto --go_opt=paths=source_relative \
    	--go-grpc_out=./genproto --go-grpc_opt=paths=source_relative \
		proto/*.proto

gen1:
	protoc -I . \
		-I thirdparty/googleapis \
		--grpc-gateway_out ./genproto \
    	--grpc-gateway_opt logtostderr=true \
    	--grpc-gateway_opt paths=source_relative \
		--go_out=./genproto --go_opt=paths=source_relative \
		--go-grpc_opt=require_unimplemented_servers=false \
    	--go-grpc_out=./genproto --go-grpc_opt=paths=source_relative \
    	proto/*.proto

install:
	go install \
    	github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-grpc-gateway \
    	github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-openapiv2 \
    	google.golang.org/protobuf/cmd/protoc-gen-go \
		google.golang.org/grpc/cmd/protoc-gen-go-grpc

buildlinux:
	docker build -t audi . --platform=linux/amd64