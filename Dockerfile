FROM golang:1.18.2-alpine as builder
WORKDIR /app
COPY . /app
ENV GOBIN "${GOPATH}/bin"
ENV GOINSECURE="gitlab.com/shop-system2/*" 
ENV GONOPROXY="gitlab.com/shop-system2/*" 
ENV GONOSUMDB="gitlab.com/shop-system2/*" 
ENV GOPRIVATE="gitlab.com/shop-system2/*" 
ENV BUILD_TAG 1.0.0
ENV GO111MODULE on
ENV CGO_ENABLED=0
ENV GOOS=linux
RUN go mod vendor
RUN go build -o audi /app/cmd/server/main.go
FROM golang:1.16.3
WORKDIR /app
COPY --from=builder /app/audi /app/audi.go
CMD ["./audi.go"]
